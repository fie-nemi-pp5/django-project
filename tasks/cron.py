import datetime

from .mail import notify_deadline
from datetime import date
import time
from .models import Task


def check_deadline():
    while True:
        # schedule next notification
        now = datetime.datetime.now()
        tomorrow = now + datetime.timedelta(days=1)
        tomorrow = datetime.datetime(year=tomorrow.year, month=tomorrow.month, day=tomorrow.day, hour=8)
        time.sleep((tomorrow - now).seconds)

        # notify
        tasks = Task.objects.filter(date_completed__isnull=True, date_planned=date.today())
        emails_to_notify = set()
        for task in tasks:
            emails_to_notify.add(task.user.email)
        for email in emails_to_notify:
            notify_deadline(email)


def startup_deadline_notification_thread():
    time.sleep(60)
    check_deadline()
