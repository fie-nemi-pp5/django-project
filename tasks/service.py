# service.py
from django.db.models import F
from .models import Task, Target
from .date_period import *
from .mail import notify_target_achieved
import datetime
import threading


def get_user_tasks(user_id: int):
    return Task.objects.filter(user_id=user_id)


def get_performance(user_id: int, period):
    user_tasks = get_user_tasks(user_id)
    today = datetime.date.today()

    tasks_done_query = user_tasks.filter(date_completed__isnull=False, date_completed__range=period)
    tasks_pending_query = user_tasks.filter(date_completed__isnull=True, date_planned__range=period)
    done = tasks_done_query.filter(date_planned__gte=F('date_completed')).count()
    late = tasks_done_query.filter(date_planned__lt=F('date_completed')).count()
    pending = tasks_pending_query.filter(date_planned__gte=today).count()
    delayed = tasks_pending_query.filter(date_planned__lt=today).count()
    return done, late, pending, delayed


def get_targets(user_id: int):
    user_targets = Target.objects.filter(user_id=user_id)
    weekly_target = user_targets.filter(target_type='weekly')
    weekly_target = weekly_target[0].target_number if weekly_target.count() != 0 else 0
    monthly_target = user_targets.filter(target_type='monthly')
    monthly_target = monthly_target[0].target_number if monthly_target.count() != 0 else 0
    semester_target = user_targets.filter(target_type='semester')
    semester_target = semester_target[0].target_number if semester_target.count() != 0 else 0
    return {'weekly_target': weekly_target, 'monthly_target': monthly_target, 'semester_target': semester_target}


def _notify_if_target_reached(task: Task, target: int, period, period_name: str):
    done, late, pending, delayed = get_performance(task.user_id, period)
    if done + late == target:
        # notify_target_achieved(period_name, target, task.user.email)
        threading.Thread(target=notify_target_achieved, daemon=True,
                         args=(period_name, target, task.user.email)).start()


def check_performance_target(task: Task):
    targets = get_targets(task.user_id)
    week_target = targets['weekly_target']
    month_target = targets['monthly_target']
    semester_target = targets['semester_target']
    if week_target > 0:
        week_period = WeekPeriod(task.date_completed).get_period()
        _notify_if_target_reached(task, week_target, week_period, "weekly")
    if month_target > 0:
        month_period = MonthPeriod(task.date_completed).get_period()
        _notify_if_target_reached(task, month_target, month_period, "monthly")
    if semester_target > 0:
        semester_period = SemesterPeriod(task.date_completed).get_period()
        _notify_if_target_reached(task, semester_target, semester_period, "semester")
