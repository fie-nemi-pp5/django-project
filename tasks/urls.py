from django.urls import path
from tasks import views

urlpatterns = [
    path('tasks/<int:input_id>', views.TaskDetailsView.as_view(), name='task_details'),
    path('tasks/', views.TasksView.as_view(), name='tasks'),
    path('tasks/edit/<int:input_id>', views.EditTaskView.as_view(), name='edit_task'),
    path('tasks/done/<int:input_id>', views.TaskCompleteView.as_view(), name='complete_task'),
    path('tasks/delete/<int:input_id>', views.TaskDeleteView.as_view(), name='delete_task'),
    path('tasks/add_image/<int:task_id>', views.AddImageView.as_view(), name='add_image'),
    path('img/delete/<int:img_id>', views.RemoveImageView.as_view(), name='remove_image'),
    path('history/', views.HistoryView.as_view(), name='history'),
    path('performance/<str:period>/<int:year>/<int:month>/<int:day>/',
         views.PerformanceView.as_view(), name='performance_date'),
    path('performance/', views.PerformanceView.as_view(), name='performance'),
    path('targets/', views.TargetsView.as_view(), name='targets'),
    path('accounts/signup/', views.SignupView.as_view(), name='signup')
]
