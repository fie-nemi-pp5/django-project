from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class NewTaskForm(forms.Form):
    title = forms.CharField(label='Title', max_length=30, required=True)
    body = forms.CharField(label='Body', max_length=200, widget=forms.Textarea, required=True)
    date_planned = forms.DateField(label='Due Date', widget=forms.DateInput(attrs={'type': 'date'}), required=True)


class ImageUploadForm(forms.Form):
    image = forms.ImageField()


class TargetsForm(forms.Form):
    weekly_target = forms.IntegerField()
    monthly_target = forms.IntegerField()
    semester_target = forms.IntegerField()


class SignupForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = UserCreationForm.Meta.fields + ('email',)
