from django.test import TestCase, Client
from django.contrib.auth.models import User
import datetime
from .models import Task, Target
from django.urls import reverse
from .service import get_performance, get_targets
from .date_period import *


g_user = 'test_user'
g_email = 'test@gmail.com'
g_password = 'test_password'


# Create your tests here.
class UserCreationTest(TestCase):
    def test_user_creation(self):
        user = User(username=g_user, password=g_password, email=g_email)
        user.save()

        self.assertIsNotNone(user)
        self.assertEqual(user.username, g_user)
        self.assertEqual(user.email, g_email)


class TaskCreationTest(TestCase):
    def setUp(self):
        # Create a test user
        self.user = User(username=g_user, password=g_password)
        self.user.save()

    def test_create_task(self):
        year = 2023
        month = 12
        day = 25
        new_task = Task(
            title='Test Task',
            body='This is a test task',
            date_planned='-'.join([str(year), str(month), str(day)]),
            user=self.user  # Assign the user to the task
        )
        new_task.save()

        retrieved_task = Task.objects.get(title='Test Task')

        self.assertEqual(retrieved_task.title, 'Test Task')
        self.assertEqual(retrieved_task.body, 'This is a test task')
        self.assertEqual(retrieved_task.date_planned, datetime.date(year, month, day))
        self.assertEqual(retrieved_task.user, self.user)


class SuperTaskMisc(TestCase):
    def setUp(self):
        self.user = User(username=g_user, password=g_password, email=g_email)
        self.user.save()
        self.task = Task(title='Test', body='Test body', date_planned='2023-12-25', user=self.user)
        self.task.save()
        self.client = Client()
        self.client.force_login(user=self.user)

    def complete(self):
        return self.client.post(reverse('complete_task', kwargs={'input_id': self.task.id}))

    def delete_task(self):
        return self.client.post(reverse('delete_task', kwargs={'input_id': self.task.id}))

    def modify(self):
        modified_data = {
            'title': 'Modified Title',
            'body': 'Modified Body',
            'date_planned': '2023-12-10'
        }

        return self.client.post(reverse('edit_task', kwargs={'input_id': self.task.id}), modified_data, follow=True)


class TaskMiscTest(SuperTaskMisc):
    def test_complete_task(self):
        response = self.complete()
        self.assertEqual(Task.objects.get(id=self.task.id).date_completed, datetime.date.today())

    def test_delete_task(self):
        task_id = self.task.id
        response = self.delete_task()
        tasks_queried = Task.objects.filter(id=task_id)
        self.assertEqual(len(tasks_queried), 0)

    def test_tasks_list_display(self):
        response = self.client.get(reverse('tasks'))
        html_content = response.content.decode('utf-8')
        self.assertIn(reverse('task_details', kwargs={'input_id': self.task.id}), html_content)
        self.assertIn(reverse('complete_task', kwargs={'input_id': self.task.id}), html_content)
        self.assertIn(reverse('delete_task', kwargs={'input_id': self.task.id}), html_content)
        self.assertIn(self.task.title, html_content)

    def test_edit_task_form_display(self):
        response = self.client.get(reverse('edit_task', kwargs={'input_id': self.task.id}))
        html_content = response.content.decode('utf-8')
        self.assertIn(self.task.title, html_content)
        self.assertIn(self.task.body, html_content)
        self.assertIn(str(self.task.date_planned), html_content)

    def test_edit_task_form_modification(self):
        response = self.modify()

        modified = Task.objects.filter(id=self.task.id)[0]
        self.assertNotEqual(modified.title, self.task.title)
        self.assertNotEqual(modified.body, self.task.body)
        self.assertNotEqual(str(modified.date_planned), self.task.body)


class PreventUnauthorizedAccessTest(SuperTaskMisc):
    def setUp(self):
        super().setUp()
        self.user_b = User(username='UserB', password='passwordB', email='userb@gmail.com')
        self.user_b.save()
        self.client.force_login(user=self.user_b)

    def assertUnmodifiedTask(self):
        queried_task = Task.objects.filter(id=self.task.id)[0]
        self.assertEqual(self.task, queried_task)

    def test_unauthorized_completion(self):
        self.complete()
        self.assertUnmodifiedTask()

    def test_unauthorized_deletion(self):
        self.delete_task()
        self.assertUnmodifiedTask()

    def test_unauthorized_modification(self):
        self.modify()
        self.assertUnmodifiedTask()


class HistoryTest(SuperTaskMisc):
    def setUp(self):
        super().setUp()
        self.complete()

    def test_history(self):
        completed_tasks = Task.objects.filter(user_id=self.user.id, date_completed__isnull=False)
        response = self.client.get(reverse('history'))
        html_content = response.content.decode('utf-8')
        for completed_task in completed_tasks:
            self.assertIn(completed_task.title, html_content)


def _innerNumbers(html_sample):
    chars = []
    is_tag = False
    for char in html_sample:
        if is_tag:
            if char == '>':
                is_tag = False
                chars.append(' ')
        else:
            if char == '<':
                is_tag = True
            else:
                if char.isdigit():
                    chars.append(char)
    result = "".join(chars).split(" ")
    return [int(number) for number in result if number.isdigit()]


class PerformanceTest(SuperTaskMisc):
    def setUp(self):
        super().setUp()

    def test_performance(self):
        response = self.client.get(reverse('performance'))
        html_content = response.content.decode('utf-8')
        substring_start = html_content.find("<hr>")
        performance = _innerNumbers(html_content[substring_start + 4:])
        done, late, pending, delayed = get_performance(self.user.id, WeekPeriod(datetime.date.today()).get_period())
        self.assertEqual(done, performance[0])
        self.assertEqual(late, performance[1])
        self.assertEqual(pending, performance[2])
        self.assertEqual(delayed, performance[3])

    def test_invalid_date(self):
        response = self.client.get('/performance/w/2023/31/31')
        self.assertEqual(301, response.status_code)


class TargetsTest(SuperTaskMisc):
    def setUp(self):
        super().setUp()
        self.target = Target(target_type='weekly', target_number=7, user=self.user)
        self.target.save()

    def test_see_targets(self):
        targets = get_targets(self.user.id)
        self.assertEqual(self.target.target_number, targets['weekly_target'])
        self.assertEqual(0, targets['monthly_target'])
        self.assertEqual(0, targets['semester_target'])

    def test_target_change(self):
        targets = get_targets(self.user.id)
        new_targets = {
            'weekly_target': 3,
            'monthly_target': 12,
            'semester_target': 72
        }
        response = self.client.post(reverse('targets'), new_targets)
        new_targets = get_targets(self.user.id)
        self.assertNotEqual(targets, new_targets)
