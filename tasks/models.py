from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import pre_delete
from django.dispatch import receiver


#class User(models.Model):
#    user_name = models.CharField(max_length=30, unique=True)
#    password = models.CharField(max_length=128)
#    email = models.EmailField(unique=True)


class Task(models.Model):
    title = models.CharField(max_length=30)
    body = models.CharField(max_length=200)
    date_planned = models.DateField()
    date_completed = models.DateField(null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, related_name='tasks')

    def __str__(self):
        return f"{self.user.username}'s {self.title} task"


class Target(models.Model):
    TYPE_OPTIONS = (
        ('weekly', 'Weekly'),
        ('monthly', 'Monthly'),
        ('semester', 'Semester')
    )

    target_type = models.CharField(max_length=10, choices=TYPE_OPTIONS)
    target_number = models.PositiveIntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='targets')

    def __str__(self):
        return f"{self.user.username}'s {self.target_type} target"


class Image(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    image = models.ImageField(null=True)

    def __str__(self):
        return f"Image {self.id}"


@receiver(pre_delete, sender=Image)
def image_delete(sender, instance, **kwargs):
    # Delete the image file from the storage when the model instance is deleted
    if instance.image:
        instance.image.delete()
