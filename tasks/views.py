# views.py
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.views import View
from .models import Task, Target, Image
from .forms import NewTaskForm, ImageUploadForm, TargetsForm, SignupForm
from .date_period import *
from .mail import notify_new_task
from .service import *
import threading
from .cron import startup_deadline_notification_thread
from django.urls import reverse


class IndexView(View):
    def get(self, request):
        return render(request, 'index.html')


class TasksView(View):
    template_name = 'tasks.html'

    def get(self, request):
        if request.user.is_authenticated:
            tasks = get_user_tasks(request.user.id).filter(date_completed__isnull=True)
            return render(request, self.template_name, {'tasks': tasks, 'new_task_form': NewTaskForm()})
        else:
            return redirect('/accounts/login/')

    def post(self, request):
        if request.user.is_authenticated:
            form = NewTaskForm(request.POST)
            if form.is_valid():
                title = form.cleaned_data.get('title')
                body = form.cleaned_data.get('body')
                date_planned = form.cleaned_data.get('date_planned')
                task = Task(title=title, body=body, date_planned=date_planned, user_id=request.user.id)
                task.save()
                threading.Thread(target=notify_new_task, daemon=True,
                                 args=(task, get_object_or_404(User, pk=request.user.id).email)).start()
            return redirect('../tasks/')
        else:
            return redirect('/accounts/login')


class TaskDetailsView(View):
    def get(self, request, input_id):
        if request.user.is_authenticated:
            task = Task.objects.filter(id=input_id)
            if len(task) == 0 or request.user.id != task[0].user_id:
                return redirect('/tasks/')
            else:
                form = ImageUploadForm()
                images = Image.objects.filter(task_id=input_id)
                return render(request, 'taskdetails.html', {'task': task[0], 'form': form, 'images': images})
        else:
            return redirect('/accounts/login/')


class TaskCompleteView(View):
    def post(self, request, input_id):
        if request.user.is_authenticated:
            task = get_object_or_404(Task, pk=input_id)
            if request.user.id == task.user_id:
                task.date_completed = date.today()
                task.save()
                check_performance_target(task)
                return redirect('/tasks/')
            else:
                raise Http404("Task not found")
        else:
            return redirect('accounts/login/')


class TaskDeleteView(View):
    def post(self, request, input_id):
        if request.user.is_authenticated:
            task = get_object_or_404(Task, pk=input_id)
            if request.user.id == task.user_id:
                task.delete()
                return redirect('/tasks/')
            else:
                raise Http404("Task not found")
        else:
            return redirect('/accounts/login/')


class EditTaskView(View):
    def get(self, request, input_id):
        if request.user.is_authenticated:
            task = get_object_or_404(Task, pk=input_id)
            if request.user.id != task.user_id:
                raise Http404("Task not found")
            else:
                edit_task_form = NewTaskForm(initial={
                    'title': task.title,
                    'body': task.body,
                    'date_planned': task.date_planned,
                })
                return render(request, 'edittask.html', {'edit_task_form': edit_task_form})
        else:
            return redirect('/accounts/login/')

    def post(self, request, input_id):
        if request.user.is_authenticated:
            task = get_object_or_404(Task, pk=input_id)
            if request.user.id == task.user_id:
                form = NewTaskForm(request.POST)
                if form.is_valid():
                    task.title = form.cleaned_data['title']
                    task.body = form.cleaned_data['body']
                    task.date_planned = form.cleaned_data['date_planned']
                    task.save()
                    return redirect('/tasks/' + str(input_id))
                else:
                    return render(request, 'edittask.html', {'edit_task_form': form})
            else:
                raise Http404("Task not found")


class AddImageView(View):
    def post(self, request, task_id):
        if request.user.is_authenticated:
            task = get_object_or_404(Task, pk=task_id)
            if task.user_id == request.user.id:
                form = ImageUploadForm(request.POST, request.FILES)
                if form.is_valid():
                    image = Image(task=task, image=form.cleaned_data['image'])
                    image.save()
                    return redirect('/tasks/' + str(task_id))

            else:
                return redirect('/tasks/')
        else:
            return redirect('/accounts/login/')


class RemoveImageView(View):
    def post(self, request, img_id):
        if request.user.is_authenticated:
            img = get_object_or_404(Image, pk=img_id)
            task_id = img.task_id
            if img.task.user_id == request.user.id:
                img.delete()
                return redirect('/tasks/' + str(task_id))
            else:
                return redirect('/tasks/')
        else:
            return redirect('/accounts/login/')


class HistoryView(View):
    def get(self, request):
        if request.user.is_authenticated:
            tasks = get_user_tasks(request.user.id).filter(date_completed__isnull=False)
            return render(request, 'history.html', {'tasks': tasks})
        else:
            return redirect('/accounts/login/')


class PerformanceView(View):
    def get(self, request, period='w', year=date.today().year, month=date.today().month, day=date.today().day):
        if request.user.is_authenticated:
            try:
                input_date = date(year, month, day)
            except ValueError:
                return redirect(reverse('performance'))
            if period == 'w':
                current_period = WeekPeriod(input_date)
            elif period == 'm':
                current_period = MonthPeriod(input_date)
            else:
                current_period = SemesterPeriod(input_date)
            this_period = current_period.get_period()

            done, late, pending, delayed = get_performance(request.user.id, this_period)

            return render(request, 'performance.html', {'period_tag': period, 'start_period': this_period[0],
                                                        'end_period': this_period[1],
                                                        'done': done, 'pending': pending, 'delayed': delayed,
                                                        'late': late})
        else:
            return redirect('/accounts/login/')


class TargetsView(View):
    def get(self, request):
        if request.user.is_authenticated:
            user_targets = get_targets(request.user.id)
            return render(request, 'targets.html', {'target_form': TargetsForm(initial=user_targets)})
        else:
            return redirect('/accounts/login/')

    def post(self, request):
        if request.user.is_authenticated:
            form = TargetsForm(request.POST)
            if form.is_valid():
                Target.objects.filter(user_id=request.user.id).delete()
                target_number_weekly = form.cleaned_data['weekly_target']
                target_number_monthly = form.cleaned_data['monthly_target']
                target_number_semester = form.cleaned_data['semester_target']
                target_week = Target(target_type='weekly', target_number=target_number_weekly, user_id=request.user.id)
                target_week.save()
                target_month = Target(target_type='monthly', target_number=target_number_monthly,
                                      user_id=request.user.id)
                target_month.save()
                target_semester = Target(target_type='semester', target_number=target_number_semester,
                                         user_id=request.user.id)
                target_semester.save()
            return redirect('../targets/')
        else:
            return redirect('/accounts/login/')


class SignupView(View):
    def get(self, request):
        return render(request, 'registration/signup.html', {'form': SignupForm()})

    def post(self, request):
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('/tasks/')
        return render(request, 'registration/signup.html', {'form': form})


deadline_notification_thread = threading.Thread(target=startup_deadline_notification_thread, daemon=True)
deadline_notification_thread.start()
