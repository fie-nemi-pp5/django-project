from datetime import date, timedelta


class Period:
    def __init__(self, input_date):
        self.input_date = input_date

    def get_period(self):
        # Default implementation
        return self.input_date

    def format_period(self):
        this_period = self.get_period()
        date_format = "%a %b %d %Y"
        return this_period[0].strftime(date_format), this_period[1].strftime(date_format)


class WeekPeriod(Period):
    def get_period(self):
        difference = (self.input_date.weekday() + 1) % 7
        start_date = self.input_date - timedelta(days=difference)
        end_date = self.input_date + timedelta(days=(6 - difference))
        return start_date, end_date


class MonthPeriod(Period):
    def get_period(self):
        start_date = date(self.input_date.year, self.input_date.month, 1)
        if self.input_date.month < 12:
            next_month = date(self.input_date.year, self.input_date.month + 1, 1)
        else:
            next_month = date(self.input_date.year + 1, 1, 1)
        return start_date, next_month - timedelta(days=1)


class SemesterPeriod(Period):
    def get_period(self):
        if self.input_date.month < 7:
            return date(self.input_date.year, 1, 1), date(self.input_date.year, 6, 30)
        else:
            return date(self.input_date.year, 7, 1), date(self.input_date.year, 12, 31)

