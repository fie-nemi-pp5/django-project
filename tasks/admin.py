from django.contrib import admin
from .models import Task, Image, Target

# Register your models here.
admin.site.register(Task)
admin.site.register(Image)
admin.site.register(Target)
