from django.core.mail import send_mail
from .models import Task, Target

sender = 'jnemi@fie.undef.edu.ar'


def notify_new_task(task: Task, recipient):
    send_mail(
        "New Task",
        "You have been assigned to: " + task.title + "\nDue date: " + str(task.date_planned),
        sender,  # From email address
        [recipient],
        fail_silently=True,  # Set it to True to suppress errors
    )


def notify_target_achieved(target_type, target_number, recipient):
    send_mail(
        target_type + " target reached",
        f"You have just reached your {target_type} target of {target_number}",
        sender,
        [recipient],
        fail_silently=True
    )


def notify_deadline(recipient):
    send_mail(
        "Deadline Notification",
        "You have tasks that are due to today",
        sender,
        [recipient],
        fail_silently=True
    )
